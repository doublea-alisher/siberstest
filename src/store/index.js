import Vue from 'vue';
import Vuex from 'vuex';

//initialisation Vue Store
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    users: [],
  },
  getters: {
    users: state => state.users,
  },
  actions: {
    getUsers({ state, dispatch, commit }) {
      //request to the server to get users
      axios.get('users').then(response => {
        //sorting contacts with alphabet and group by first letter
        let users = response.data.sort((userA, userB) => {
          if (userA.name < userB.name) {
            return -1;
          } else {
            return 1
          }
        });
        let groupedUsers = _.groupBy(users, user => {
          return user.name[0];
        });
        users = [];
        for (let group in groupedUsers) {
          if (groupedUsers.hasOwnProperty(group)) {
            users = [
              ...users,
              { group: group.toUpperCase(), name: '------', phone: '-------', email: '------' },
              ...groupedUsers[group],
            ]
          }
        }
        commit('setUsers', users);
      });
    }
  },
  mutations: {
    setUsers(state, list) {
      //set state's parameter 'users'
      state.users = list;
    }
  },
});