import Vue from 'vue';
import App from './App.vue';
//import store
import store from './store';
//import router
import router from './router';
//import plugins
import './plugins/axios';
import './plugins/element_ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
