import Vue from 'vue';
import Router from 'vue-router';
//import the components that the router will show
import Contacts from '../components/Contacts';


Vue.use(Router);

const title = 'Sibers';
//setting Router params and add new routes
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Contacts',
      component: Contacts,
      meta: {
        title: 'Контакты',
      },
    },
    {
      path: '/*',
      redirect: {
        name: 'Contacts'
      },
    }
  ],
});

router.beforeEach((to, from, next) => {
  //setting Router. When we go to page, page's title will be change on title in route
  if (to.meta && to.meta.title) {
    document.title = `${to.meta.title} | ${title}`;
  }
  next();
});

export default router;