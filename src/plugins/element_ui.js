import Vue from 'vue';
import lang from 'element-ui/lib/locale/lang/ru-RU';
import locale from 'element-ui/lib/locale';
//setting default locale
locale.use(lang);

//import only need elements from library. It makes build easy
import {
  Input,
  Table,
  TableColumn,
  Button,
  Loading,
  Dialog,
  Switch,
} from 'element-ui';

Vue.use(Input);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Button);
Vue.use(Loading);
Vue.use(Dialog);
Vue.use(Switch);