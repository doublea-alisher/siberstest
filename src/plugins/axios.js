import axios from 'axios';
import Vue from "vue";

//setting default settings for requests
const _axios = axios.create({
  baseURL: ' http://demo.sibers.com', //Base url. Where we'll send requests
  withCredentials: false, // Check cross-site Access-Control
});

Vue.axios = _axios;
window.axios = _axios;